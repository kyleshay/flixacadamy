To run locally, run 'npm install', then 'node app.js', and open your browser to 'localhost:8081'.

To deploy, simply commit and push.

There is a post hook on the bitbucket repo (https://bitbucket.org/kyleshay/flixacadamy/src)
that calls some codeship scripts to run (https://www.codeship.io/projects/19808). This script is in the root of the app (deploy_beanstalk.sh).

The script uploads the repo to our s3 instance (https://s3.amazonaws.com/elasticbeanstalk-us-east-1-274905981761/assets/), and deploys the app
to a node environemnt in aws elastic beanstalk (http://flixapp-en.elasticbeanstalk.com/). The MySql instance is on amazon RDS (see app.js for connection info).

We're also using AWS Route53 in order to point domains at beanstalk apps. Basically, you update the nameservers with the DNS provider, and
create a set/map to the aliased domain in Route53 to your beanstalk app id. Right we have maps for flixapp.co and www.flixapp.co.

See documentation.txt for info on api.