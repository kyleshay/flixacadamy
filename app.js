var express = require('express'),
	mysql = require('mysql'),
	knox = require('knox'),
	mpu = require('knox-mpu'),
	Busboy = require('busboy'),
	app = express();

app.set('port', process.env.PORT || 8081); 

console.log('flix server started and is listening on port: ' + app.get('port'));

// load index.html app
app.use(express.static(__dirname));

app.use(app.router);

app.use("/img", express.static(__dirname + '/img'));
app.use(express.static(__dirname + '/img'));
app.use("/bower_components", express.static(__dirname + '/bower_components'));
app.use(express.static(__dirname + '/bower_components'));

// configure knox for s3
var client = knox.createClient({
	key      : 'AKIAIJADHNKWSHRUZXLA',
	secret   : '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',
	bucket   : 'flix.syllabus'
});

// init database
var connection = mysql.createConnection({
	host     : 'aa3npbar8mc69g.cvsce6jmgxz0.us-east-1.rds.amazonaws.com',
	user     : 'flixmaster',
	password : 'flixpass227',
	database : 'ebdb',
	port     : '3306'
});
connection.connect();

//----- API
// return video
app.get('/api/video', function (req, res) {
	var q = [];
	if(req.query.title) q.push("Video.title like '%"+req.query.title+"%'");
	if(req.query.year) q.push('Video.year='+req.query.year);
	if(req.query.age_min) q.push('Video.age_min is null or Video.age_min>='+req.query.age_min);
	if(req.query.age_max) q.push('Video.age_max is null or Video.age_max<='+req.query.age_max);
	if(req.query.captioned == 'true') q.push('Video.captioned=true');
	if(req.query.subtopic && req.query.subtopic != 'none') q.push("Subtopic.name='"+req.query.subtopic+"'");
	if(req.query.topic && req.query.topic != 'none') q.push("Topic.name='"+req.query.topic+"'");
	if(req.query.rating) q.push('Rating.name in (\''+req.query.rating.replace(',',"','")+'\')');
	if(req.query.runtime && !isNaN(req.query.runtime)) {
		var low = 0, high = 2147483647;
		switch(req.query.runtime) {
			case "5": high = 10; break;
			case "15": low = 9; high = 21; break;
			case "30": low = 18; high = 42;break;
			case "60": low = 36; break;
		}
		q.push('Video.runtime between '+low+' and '+high);
	}
	if(q.length) q = ' where ' + q.join(' and ') + ' ';

	// looking for:
	console.log('search request for: ' + q);

	connection.query({
	sql:
		" SELECT " +
		"  Video.id," +
		"  Video.title," +
		"  Video.director," +
		"  Video.runtime," +
		"  Video.year," +
		"  Video.image_url," +
		"  Video.captioned," +
		"  Video.age_min," +
		"  Video.age_max," +
		"  Rating.name," +
		"  Subtopic.name," +
		"  Affiliate.*," +
		"  Syllabus.*" +
		" FROM" +
		"  Video" +
		" LEFT JOIN Rating ON Video.rating_id = Rating.id" +
		" LEFT JOIN Topic ON Video.topic_id = Topic.id" +
		" LEFT JOIN Subtopic ON Video.subtopic_id = Subtopic.id" +
		" LEFT JOIN Language ON Video.language_id = Language.id" + 
		" LEFT JOIN Affiliate ON Video.id = Affiliate.video_id" +
		" LEFT JOIN Syllabus ON Video.id = Syllabus.video_id" + q +
		" ORDER BY Video.id",
	nestTables: true },
		function(err, rows, fields) {
			if (err) throw err;

			var results = [], id = 0, add = false;
			for(var r = 0; r < rows.length-1; r++) {
				if(rows[r].Video.id != id) {
					id = rows[r].Video.id;
					results.push({
						id: id,
						title: rows[r].Video.title,
						director: rows[r].Video.director,
						runtime: rows[r].Video.runtime,
						year: rows[r].Video.year,
						image_url: rows[r].Video.image_url,
						captioned: rows[r].Video.captioned,
						age_min: rows[r].Video.age_min,
						age_max: rows[r].Video.age_max,
						rating: rows[r].Rating.name,
						subtopic: rows[r].Subtopic.name,
						affiliate: [],
						syllabus: []
					});
				}
				var current = results[results.length-1];
				if(rows[r].Affiliate.id != null) {
					add = true;
					for(var a = 0; a < current.affiliate.length; a++) {
						if(current.affiliate[a].id == rows[r].Affiliate.id) add = false;
					}
					if(add) current.affiliate.push({
						id: rows[r].Affiliate.id,
						name: rows[r].Affiliate.name,
						url: rows[r].Affiliate.url,
					});
				}
				if(rows[r].Syllabus.id != null) {
					add = true;
					for(var s = 0; s < current.syllabus.length; s++) {
						if(current.syllabus[s].id == rows[r].Syllabus.id) add = false;
					}
					if(add) current.syllabus.push({
						id: rows[r].Syllabus.id,
						url: rows[r].Syllabus.url,
						create: rows[r].Syllabus.create_dttm,
						modify: rows[r].Syllabus.modify_dttm,
					});
				}
			}

			res.send(results);
		}
	);
});

// return topic
app.get('/api/topic', function (req, res) {
	connection.query('SELECT * FROM Topic WHERE id = ' + req.query.id , function(err, rows, fields) {
		if (err) throw err;
		res.send(rows);
	});
});

//returns just a list of topic names
app.get('/api/topics', function (req, res) {
    connection.query('SELECT name FROM Topic' , function(err, rows, fields) {
        if (err) throw err;
        res.send(rows);
    });
});

// return affiliate links for a specified video id
app.get('/api/affiliate', function (req, res) {
	if(!isNaN(req.query.id) && req.query.id <= 0) {
		res.send("no id specified.");
		return;
	}
	connection.query('SELECT * from Affiliate where video_id='+req.query.id, function(err, rows, fields) {
		if (err) throw err;
		res.send(rows);
	});
});

app.get('/api/syllabus', function (req, res) {
	// invalid request
	if(!isNaN(req.query.id) && req.query.id <= 0) {
		res.send("no id specified.");
		return;
	}
	// return any syllabus for the given video id
	connection.query('SELECT * from Syllabus where video_id='+req.query.id, function(err, rows, fields) {
		if (err) throw err;
		res.send(rows);
	});
});

app.post('/api/syllabus/add', function (req, res) {
	    var busboy = new Busboy({ headers: req.headers });
	    busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
			upload = new mpu({client: client, objectName: req.query.id + '/' + filename, stream: file}, function(err, body) {
				if (err) throw err;
				console.log(filename + ' has been uploaded.');
				res.end();
			});
	    });
	    req.pipe(busboy);
});

app.get('/api/syllabus/get', function (req, res) {
	if(!req.query.video_id && !req.query.file) {
		res.send("missing some params");
		return;
	}
	client.getFile('/'+req.query.id+'/'+req.query.file, function(err, knoxres) {
		if (err) throw err;

		knoxres.pipe(res);
	});
});

app.get('/api/rating', function (req, res) {
	connection.query('SELECT * from Rating', function(err, rows, fields) {
		if (err) throw err;
		res.send(rows);
	});
});

app.get('/api/language', function (req, res) {
	connection.query('SELECT * from Language', function(err, rows, fields) {
		if (err) throw err;
		res.send(rows);
	});
});

//returns all subtopics
app.get('/api/subtopic', function (req, res) {
	connection.query('SELECT * from Subtopic where id=' + req.query.id, function(err, rows, fields) {
		if (err) throw err;
		res.send(rows);
	});
});

// return just a list of names for subtopics that match a given topic by name
app.get('/api/subtopics', function (req, res) {
    connection.query('SELECT Subtopic.name FROM Subtopic, Topic WHERE Subtopic.topic_id = Topic.id AND Topic.name = "' + req.query.name + '"', function(err, rows, fields) {
        if (err) throw err;
        res.send(rows);
    });
});

// Launch server
app.listen(8081);
