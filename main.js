var topicsArray = [], searchTerms;

$('.tab').on('click', togglePane)

$('.info-icon').on('mouseenter', tooltipThis).on('mouseleave', destroyTooltip)

$('.expander').on('click', toggleSection)

$('.search-button').on('click', search)

$('.search-again-button').on('click', resetSearch)

$('#subject').autocomplete({
    source: topicsArray,
    select: function( event, ui ) {
        event.preventDefault();
        $('.search-pane #subject').val(ui.item.label)
        getSubtopics(ui.item.label);
    }
});

$(document).on('click', '.search-result', showResultDetails)


var tooltipMessages = {
    "subject" :  "A major topic or subject",
    "subtopics" : "A related subtopic to the subject",
    "age" :      "Restrict results to an age range",
    "runtime" :  "Only find results under a given runtime",
    "rating" :   "Only allow results up to a rating",
    "language" : "The language the video is recorded in",
    "cc" :       "Find only closed-captioned videos",
    "source" :   "A valid web address/link to a video",
    "upload" :   "Upload a study guide in PDF format"
}


function togglePane(){
    if (!$(this).hasClass('active')){
        $('.tab').toggleClass('hidden active');
        $('.pane').toggleClass('hidden active');
    }
}

function showSubtopicField(){
    $('#subtopics').parent().show();
}

function tooltipThis(){
    var tip = $(this).data('tip');
    var message = tooltipMessages[tip];
    var tooltip = $('#tooltip-template').clone().appendTo('body');
    tooltip.find('.message').text(message);
    tooltip.addClass('tooltip-clone').show().css({
        top : $(this).position().top - 10,
        left : $(this).position().left + 30
    })
}

function destroyTooltip(){
    $('.tooltip-clone').empty().remove();
}

function toggleSection(){
    $(this).toggleClass('expanded')
    $(this).parent().find('.expandable').toggleClass('expanded collapsed')
}

function search(){
    searchTerms = getSearchParameters();
    findEntries(searchTerms).then(function(results){
        var context = {
            searchTerms : searchTerms,
            results : results
        }
        var source   = $("#search-result-template").html();
        var template = Handlebars.compile(source);
        var html = template(context);
        $('.results-bucket').html(html);
        $('.search-pane').addClass('results-added');
        var to = $('.results-bucket').position().top;

        $('body').animate({scrollTop: to }, 250)
    })
}

function findEntries(searchTerms){
    return $.ajax({
      url: "/api/video",
      data : searchTerms
    });
}

function getSearchParameters(){

    return searchTerms = {
        topic : $('.search-pane #subject').val(),
        subtopic : $('.search-pane #subtopics').val(),
        age_min : $('.search-pane #age-min').val(),
        age_max : $('.search-pane #age-max').val(),
        runtime : $('.search-pane #run-time').val(),
        language : $('.search-pane #language').val(),
        captioned : $('.search-pane #cc').is(':checked')
    };
}

function getAllTopics(){

    $.ajax({ url: "/api/topics"}).then(function(topics) {
        for (var i = topics.length - 1; i >= 0; i--) {
            topicsArray.push(topics[i].name);
        };
    });
}

function getSubtopics(topicName){
    
    $.ajax({
        url: "/api/subtopics?name="+topicName
    }).then(function(response) {
        renderSubtopics(response);
    });
}

function renderSubtopics(subtopics){
    var html = '<option value="none">Select One</option>';
    for (var i = subtopics.length - 1; i >= 0; i--) {
        html += '<option>'+ subtopics[i].name +'</option>'
    };

    $('#subtopics').html(html).parent().removeClass('hidden');
}

function resetSearch(){
    $('.results-bucket').html('');
    $('.search-pane').removeClass('results-added');
    $('.search-pane #subject').val('');
    //$('.search-pane #subtopics').val(''),
    $('.search-pane #age-min').val('');
    $('.search-pane #age-max').val('');
    //$('.search-pane #run-time').val(),
    //$('.search-pane #language').val(),
    //$('.search-pane #cc').is(':checked')
}

function showResultDetails(){
    $(this).find('.video-details').slideToggle('fast');
}

getAllTopics();