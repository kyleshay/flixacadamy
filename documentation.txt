
/api/video
	description: returns a json array containing objects about a video.
	example:

	***All parameters are optional***
	PARAM     | DESCRIPTION                             | EXAMPLE
	-------------------------------------------------------------------------
	title     | name of the movie, partial string match | title=history
	year      | year of the movie                       | year=2013
	age_min   | int null for no min                     | age_min=13
	age_max   | int null for no max                     | age_max=21
	subtopic  | subtopic by name                        | subtopic=Astronomy
	topic     | topic by name                           | subtopic=Mathematics
	captioned | boolean true/false                      | captioned=true
	rating    | comma seperated list of ratings         | rating=G,PG,PG-13
	runtime   | 5, 15, 30, or 60                        | runtime=15


/api/rating
	description: returns a json array containing all possible ratings.
	[{id:1,name:"G"},{id:2,name:"PG"},{id:3,name:"PG-13"},{id:4,name:"R"}]

/api/language
	description: returns a json array containing all possible languages.
	[{id:1,name:"English"}]

/api/topic
	description: returns a json array containing all possible topics.
	[{id:1,name:"Science"},{id:2,name:"Mathematics"}]

/api/subtopic
	description: returns a json array containing all possible topics.
	[{id:1,topic_id:1,name:"Physics"}]
	
	PARAM     | DESCRIPTION                             | EXAMPLE
	-------------------------------------------------------------------------
	id        | id of the topic for subtopic list       | id=2

/api/affiliate
	description: returns a json array containing all possible affiliate links for a video.
	[{id:1,video_id:1,name:"Amazon",url:"http://www.amazon.com/gp/product/B000HEBCZQ/ref=as_li_ss_tl?ie=UTF8&camp=1789&creative=390957&creativeASIN=B000HEBCZQ&linkCode=as2&tag=flixacadamy-20"}]

	PARAM     | DESCRIPTION                             | EXAMPLE
	-------------------------------------------------------------------------
	id        | id of the video an affiliate link list  | id=2

/api/syllabus
	description: returns a json array containing all possible syllabus links for a video.
	[{id:3,video_id:1,url:"http://www.google.com/",create_dttm:null,modify_dttm:null},{id:4,video_id:1,url:"http://www.wikipedia.com/",create_dttm:null,modify_dttm:null}]

	PARAM     | DESCRIPTION                             | EXAMPLE
	-------------------------------------------------------------------------
	id        | id of the video an syllabus link list  | id=2

/api/syllabus/add (POST)
	description: adds a file to amazon s3 storage

	PARAM     | DESCRIPTION                             | EXAMPLE
	-------------------------------------------------------------------------
	id        | id of the video to link a syllabus to   | id=2
	file      | the file posted                         | file=FILEDATAFROMAFORM?

/api/syllabus/get (GET)
	description: gets a file from amazon s3 storage
	example: http://localhost:8081/api/syllabus/get?video_id=test&file=lol.png

	PARAM     | DESCRIPTION                             | EXAMPLE
	-------------------------------------------------------------------------
	id        | id of the video to find                 | id=2
	file      | the file name & extension posted        | file=asdf.pdf
