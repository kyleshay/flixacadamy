

CREATE TABLE Topic (
	id int NOT NULL AUTO_INCREMENT,
	name varchar(255) NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE Subtopic (
	id int NOT NULL AUTO_INCREMENT,
	topic_id int NOT NULL,
	name varchar(255) NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (topic_id) REFERENCES Topic(id)
);

CREATE TABLE Language (
	id int NOT NULL AUTO_INCREMENT,
	name varchar(255) NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE Rating (
	id int NOT NULL AUTO_INCREMENT,
	name varchar(255) NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE User (
	id int NOT NULL AUTO_INCREMENT,
	name varchar(1024) NOT NULL,
	email varchar(1024) NOT NULL,
	city varchar(512) NOT NULL,
	state varchar(2) NOT NULL,
	teacher boolean,
	language_id int,
	career_start date,
	grade_id int,
	PRIMARY KEY (id),
	FOREIGN KEY (language_id) REFERENCES Language(id)
);

CREATE TABLE Video (
	id int NOT NULL AUTO_INCREMENT,
	title varchar(255) NOT NULL,
	director varchar(255) NOT NULL,
	runtime int NOT NULL,
	year varchar(255),
	image_url varchar(2048),
	topic_id int,
	subtopic_id int,
	language_id int,
	captioned boolean,
	age_min int,
	age_max int,
	rating_id int,
	PRIMARY KEY (id),
	FOREIGN KEY (topic_id) REFERENCES Topic(id),
	FOREIGN KEY (subtopic_id) REFERENCES Subtopic(id),
	FOREIGN KEY (rating_id) REFERENCES Rating(id),
	FOREIGN KEY (language_id) REFERENCES Language(id)
);

CREATE TABLE Syllabus (
	id int NOT NULL AUTO_INCREMENT,
	video_id int NOT NULL,
	url varchar(2048) NOT NULL,
	create_dttm date,
	modify_dttm date,
	PRIMARY KEY (id),
	FOREIGN KEY (video_id) REFERENCES Video(id)
);

CREATE TABLE Affiliate (
	id int NOT NULL AUTO_INCREMENT,
	video_id int NOT NULL,
	name varchar(255) NOT NULL,
	url varchar(2048) NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (video_id) REFERENCES Video(id)
);

INSERT INTO Language (name) VALUES('English');
INSERT INTO Rating (name) VALUES('G');
INSERT INTO Rating (name) VALUES('PG');
INSERT INTO Rating (name) VALUES('PG-13');
INSERT INTO Rating (name) VALUES('R');
INSERT INTO Topic (name) VALUES('Science');
SET @topicid = LAST_INSERT_ID();
INSERT INTO Subtopic (topic_id, name) VALUES(@topicid, 'Physics');
INSERT INTO Subtopic (topic_id, name) VALUES(@topicid, 'Chemistry');
INSERT INTO Subtopic (topic_id, name) VALUES(@topicid, 'Biology');
INSERT INTO Subtopic (topic_id, name) VALUES(@topicid, 'Astronomy');
INSERT INTO Topic (name) VALUES('Mathematics');
SET @topicid = LAST_INSERT_ID();
INSERT INTO Subtopic (topic_id, name) VALUES(@topicid, 'Algebra');
INSERT INTO Subtopic (topic_id, name) VALUES(@topicid, 'Geometry');
INSERT INTO Subtopic (topic_id, name) VALUES(@topicid, 'Calculus');
INSERT INTO Video (title, director, runtime, year, image_url, subtopic_id, language_id, captioned, age_min, age_max, rating_id) VALUES('2001: A Space Odyssey', 'Stanley Kubrick', 160, 1968, 'http://ia.media-imdb.com/images/M/MV5BNDYyMDgxNDQ5Nl5BMl5BanBnXkFtZTcwMjc1ODg3OA@@._V1_SX640_SY720_.jpg', 3, 1, false, 13, null, 1);
INSERT INTO Video (title, director, runtime, year, image_url, subtopic_id, language_id, captioned, age_min, age_max, rating_id) VALUES('Space Jam', 'Joe Pytka', 88, 1996, 'http://ia.media-imdb.com/images/M/MV5BMTQ5NDg1NTgwOV5BMl5BanBnXkFtZTcwODAwNDAwMQ@@._V1_SX640_SY720_.jpg', 3, 1, false, 6, null, 2);
